#!/bin/sh
echo -n "Press a button on the Razer Kishi controller... "
evdev="$($(dirname "$0")/identify_evdev.py)"
echo using device "$evdev".

xboxdrv --evdev "$evdev" \
--evdev-keymap BTN_START=start,BTN_SELECT=back \
--evdev-keymap BTN_MODE=guide \
--evdev-keymap BTN_A=a,BTN_B=b \
--evdev-keymap BTN_NORTH=x,BTN_WEST=y \
--evdev-keymap BTN_TL=lb,BTN_TR=rb \
--evdev-keymap KEY_#312=lt,KEY_#313=rt \
--evdev-keymap BTN_THUMBL=tl,BTN_THUMBR=tr \
--evdev-absmap ABS_X=x1,ABS_Y=y1 \
--evdev-absmap ABS_Z=x2,ABS_RZ=y2 \
--evdev-absmap ABS_BRAKE=lt,ABS_GAS=rt \
--evdev-absmap ABS_HAT0X=dpad_x,ABS_HAT0Y=dpad_y \
--axismap -y1=y1,-y2=y2 \
--device-name "Razer Kishi" \
--detach-kernel-driver --mimic-xpad --silent --quiet $@