# Razer Kishi (Linux)

Using the Razer Kishi controller on linux is really hit or miss.

It uses a standard layout but it's not recognized in steam nor is it present in any standrard distro udev rules.

## Working with games on linux

### In Steam:

To play your Steam game library, go into `Settings > Controller > General Controller Parameters`.

This will open Steam Big Picture to configure a controller.

Select:

- [x] Guide button opens Steam
- [x] Assist config for Xbox
- [x] Assist generic config

You controller should popup as a xbox controller and play like it in games.

See this YouTube video on [How To Set Up & Configure Controllers to Work with Games Using Steam Proton on Linux](https://www.youtube.com/watch?v=JqCe0uzo8s4).

### RetroArch

The Kishi is fully supported in RetroArch as it can be installed on android. 

Just use [retroarch-joypad-autoconfig](https://github.com/libretro/retroarch-joypad-autoconfig).

### Wine/DXVK

Sometimes you want to play a PC game without Steam (i.e. Star Wars Jedi: Fallen Order on Origin).

You can use this project to achive so.

If you have a Razer Kishi, just `./mkdrv.sh` will prompt you to presss any button on the Kishi and will create a fake xbox controller for it.

If you have another uncommon controller, use `create_xboxdrv_evdev_map.py` to map it correcly to an xbox controller.

The last print, should be the `xboxdrv` command to run with your mapping. You can then tweak it to create a new `mkdrv.sh` for your controller.

#### How to check if it worked correctly

You can either install `jstest-gtk` which is a small GUI for `jstest` (Joystick testing) or use the [gamepad tester website](https://gamepad-tester.com).

## Contributions

This was inspired by A Weird Imagination's posts on [Emulating Xbox controllers on Linux](https://aweirdimagination.net/2015/04/06/emulating-xbox-controllers-on-linux/) and [Identifying joystick devices](https://aweirdimagination.net/2015/04/04/identifying-joystick-devices/).

I also recommend you read [Arch Linux's Wiki entry about Gamepad](https://wiki.archlinux.org/title/Gamepad) which is the most complete source of info about gamepad compatibility for Linux.